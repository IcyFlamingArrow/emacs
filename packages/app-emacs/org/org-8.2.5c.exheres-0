# Copyright 2011 Elias Pipping <pipping@exherbo.org>
# Distributed under the terms of the GNU General Public License v2

require elisp-module

SUMMARY="Mode for Notes, Project Planning, and Authoring"
HOMEPAGE="http://orgmode.org/"
DOWNLOADS="http://orgmode.org/${PNV}.tar.gz"

LICENCES="GPL-3"
SLOT="0"
PLATFORMS="~amd64"
MYOPTIONS=""

DEPENDENCIES="
    build+run:
        app-editors/emacs
        app-emacs/htmlize
"

DEFAULT_SRC_COMPILE_PARAMS=( compile )

RESTRICT="test" # Tarballs do not include the testing directory

src_configure() {
    cat <<EOF >> local.mk
EMACS = ${EMACS}
lispdir = ${ELISP_SITE_LISP}/${PN}
# Avoid requiring pdftex and texi2html installed
ORG_MAKE_DOC = info

ORG_ADD_CONTRIB = *

EOF
    # Contribs which cannot be built break src_install
    edo rm contrib/lisp/org-jira.el # Requires jira
    edo rm contrib/lisp/ob-oz.el    # Requires mozart

    # Packaged separately
    edo rm contrib/lisp/htmlize.el
}

src_install() {
    default
    elisp-install-site-file
}

